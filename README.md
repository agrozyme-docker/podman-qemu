# Summary

Source: https://gitlab.com/agrozyme-docker/podman-qemu

Alpine-based image with podman and qemu. Easily allows cross platform image builds.

# Description

Inspired By [jonoh/docker-buildx-qemu](https://hub.docker.com/r/jonoh/docker-buildx-qemu)

It's been tested with GitLab CI on gitlab.com

# Support Architectures

This Docker image can `emulate` the environments.

Use valid GOARCH values, see:

- [GOARCH](https://golang.org/doc/install/source#introduction)
- [official images architectures](https://github.com/docker-library/official-images#architectures-other-than-amd64)

# Target Architectures

The Docker image can `run` in the environment (The intersection of Alpine and QEMU support platforms):

- amd64
- 386
- arm64
- arm
- ppc64le
- riscv64

# GitLab CI Template

It has two template files for building multi-platform Docker images.

- Single: For a single branch repository, deploy `main` to `latest`
- Multiple: Each branch is mapped to a docker image tag and the repository name is the same as Docker Registry.

## Docker Hub

- Single: `/.gitlab/ci_templates/single/docker-hub.yaml`
- Multiple: `/.gitlab/ci_templates/multiple/docker-hub.yaml`

## GitLab Container Registry

- Single: `/.gitlab/ci_templates/single/gitlab.yaml`
- Multiple: `/.gitlab/ci_templates/multiple/gitlab.yaml`

# Workflow:

- The `main` tag of the docker image is used to test the build.
- The `build` job pushes the `main` tag of the docker image.
- If the `build` job is successful, then the `delpoy` job pushes the multi-platform Docker image.
- After pushing the docker image, the `delpoy` job will update the `description` of the Docker Hub repository.
  - Short description: The `description` of
    the [project](https://gitlab.com/help/user/project/settings/index.md#general-settings).
  - Full description: `README.md`

# Environment variables

Put all docker image repositories into a `Group` and
set [Variables](https://gitlab.com/help/ci/variables/README#group-level-environment-variables) in `CI / CD Settings`

## Override priority

Put the variables in `.gitlab-ci.yml` or `CI / CD Settings` to override default.

- `.gitlab-ci.yml` stage variables
- `.gitlab-ci.yml` global variables
- Project `CI / CD Settings`
- Group `CI / CD Settings`

## Required

**No default value, all variables are required. (Only for Docker Hub)**

- `DOCKER_HUB_USER`: Login username
- `DOCKER_HUB_PASS`: Access token
  - See: https://docs.docker.com/security/for-developers/access-tokens
- `DOCKER_HUB_NAMESPACE`:
  - username or organization
  - It will be passed to the `Dockerfile` as an `ARG`.

## Optional

- `DOCKER_HUB_API_URL`
  - The Docker Registry API url
  - Only use `.gitlab-ci.yml` to override
  - default: `https://hub.docker.com/v2`
- `DOCKER_REGISTRY`
  - The `docker login` server
  - Only use `.gitlab-ci.yml` to override
  - It will be passed to the `Dockerfile` as an `ARG`.
  - default: `docker.io`
- `DOCKER_ARCH`
  - The target architectures for building docker image.
  - default: `amd64`.
- `DOCKER_TAG`
  - Use for push docker image
- `DOCKER_IMAGE_TAG`
  - Same value as `DOCKER_TAG`
- `DOCKER_TAG_POSTFIX`
  - The tag postfix list for docker image

# Hook Scripts

It has two `bash` scripts to hook `before_script` and `after_script`, the scripts run at beginning and end of the docker
build and push action.

- before_script.sh
- after_script.sh

Note:

- If you want to download the files during the build of docker image, you can do this by writing `before_script.sh` and
  using `COPY` command in the `Dockerfile`.
- It is just done once before / after the `podman build` and you can speed up the build of multi-platform docker image.

# Build Jobs

It define some tasks, you can combine for your tasks

## .build-one-arch

Build the same architecture image as the CI runner for quick testing of build scripts

## .build-all-arch

Build the `DOCKER_ARCH` architecture image

## .build-update-manifest

Create and push the manifest using `DOCKER_TAG_POSTFIX`

## .build-update-metadata

Update metadata to the docker registry

# Example

This GitLab example should give you an idea of how to use the image.

.gitlab-ci.yml

```yaml
include:
  - project: agrozyme-docker/podman-qemu
    ref: main
    file: /.gitlab/ci_templates/single/docker-hub.yaml

build-one-arch:
  extends:
    - .build-one-arch

build-all-arch:
  extends:
    - .build-all-arch
  needs: ['build-one-arch']

build-update-metadata:
  extends:
    - .build-update-metadata
  needs: ['build-all-arch']
```
