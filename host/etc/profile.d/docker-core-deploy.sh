#! /bin/bash
set -euxo pipefail

function main() {
  local deploy="/home/core/deploy"
  local profile="${deploy}/profile.sh"
  export PATH="${PATH}:${deploy}"
  export DOCKER_CORE_DEPLOY="${deploy}"

  [[ -d "${deploy}" ]] && chmod +x "${deploy}"/*.sh

  # shellcheck disable=SC1090
  [[ -f "${profile}" ]] && source "${profile}"
}

main
unset main
set -euxo pipefail
