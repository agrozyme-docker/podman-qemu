#! /bin/bash
set -euxo pipefail

function add_packages() {
  sed -i -re "s/^#(http.*)$/\1/" -re "s/^(http.*\/edge\/.*)$/#\1/" /etc/apk/repositories

  # shellcheck disable=SC2046
  apk add --no-cache bash tini-static curl jq git podman skopeo $(apk search --no-cache -xq qemu-* | grep -Ev "(-audio-|-block-|-hw-|-system-|-ui-)" | grep -Ev "(-chardev-spice|-doc|-gpu|-guest-agent|-helper|-img|-lang|-modules|-openrc|-tools|-virtiofsd)$" | tr "\n" " " | sort)
  # apk add --no-cache bash tini-static curl jq podman skopeo $(apk search --no-cache -xq qemu-* | tr "\n" " " | sort)
}

function download_binfmt() {
  local version="v9.1.2"
  local folder="/usr/local/bin"
  local file="qemu-binfmt-conf.sh"
  local source="https://raw.githubusercontent.com/qemu/qemu/${version}/scripts/${file}"
  local target="${folder}/${file}"

  if [[ ! -f "${target}" ]]; then
    mkdir -p "${folder}"
    wget -qO "${target}" "${source}"
  fi

  chmod +x "${target}"
}

function build() {
  add_packages
  download_binfmt
}

function main() {
  local call="${1:-}"

  if [[ -z $(typeset -F "${call}") ]]; then
    return
  fi

  shift
  ${call} "$@"
}

main "$@"
